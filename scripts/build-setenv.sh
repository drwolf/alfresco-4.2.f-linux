#!/bin/sh
LDFLAGS="-L/opt/alfresco-4.2.f/common/lib $LDFLAGS"
export LDFLAGS
CFLAGS="-I/opt/alfresco-4.2.f/common/include $CFLAGS"
export CFLAGS
		    
PKG_CONFIG_PATH="/opt/alfresco-4.2.f/common/lib/pkgconfig"
export PKG_CONFIG_PATH
