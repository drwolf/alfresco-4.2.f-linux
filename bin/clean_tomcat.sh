#!/bin/sh
# ---------------------------------
# Script to clean Tomcat temp files
# ---------------------------------
echo "Cleaning temporary Alfresco files from Tomcat..."
rm -rf /opt/alfresco-4.2.f/tomcat/temp/Alfresco
rm -rf /opt/alfresco-4.2.f/tomcat/work/Catalina/localhost/alfresco
rm -rf /opt/alfresco-4.2.f/tomcat/work/Catalina/localhost/share
rm -rf /opt/alfresco-4.2.f/tomcat/work/Catalina/localhost/awe
rm -rf /opt/alfresco-4.2.f/tomcat/work/Catalina/localhost/wcmqs