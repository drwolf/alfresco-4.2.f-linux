JAVA_HOME=/opt/alfresco-4.2.f/java
JRE_HOME=$JAVA_HOME
JAVA_OPTS="-XX:-DisableExplicitGC -XX:ReservedCodeCacheSize=128m $JAVA_OPTS "
JAVA_OPTS="-XX:MaxPermSize=256M -Xms128M -Xmx1024M $JAVA_OPTS " # java-memory-settings
export JAVA_HOME
export JRE_HOME
export JAVA_OPTS
			    